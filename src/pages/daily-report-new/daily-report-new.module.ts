import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyReportNewPage } from './daily-report-new';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DailyReportNewPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyReportNewPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class DailyReportNewPageModule {}
