import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotifSettingPage } from './notif-setting';
import { TranslateModule } from '@ngx-translate/core';
// import { MatDatepickerModule } from '@angular/material/datepicker';
@NgModule({
  declarations: [
    NotifSettingPage,
  ],
  imports: [
    IonicPageModule.forChild(NotifSettingPage),
    TranslateModule.forChild(),
    // MatDatepickerModule
  ],
})
export class NotifSettingPageModule { }
