import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddGeofencePage } from './add-geofence';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';

@NgModule({
  declarations: [
    AddGeofencePage,
  ],
  imports: [
    IonicPageModule.forChild(AddGeofencePage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  providers: [
    // NativeGeocoder
  ]
})
export class AddGeofencePageModule {}
